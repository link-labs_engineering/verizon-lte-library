package com.linklabs.lte;

import com.google.common.collect.Lists;
import com.linklabs.lte.model.activate.*;
import com.linklabs.lte.model.common.Device;
import com.linklabs.lte.model.common.DeviceId;
import com.linklabs.lte.model.common.DeviceIdBuilder;
import com.linklabs.lte.model.deactivate.DeactivateRequest;
import com.linklabs.lte.model.list.ListRequest;
import com.linklabs.lte.model.list.ListResponse;
import com.linklabs.lte.model.restore.RestoreRequest;
import com.linklabs.lte.model.suspend.SuspendRequest;
import com.linklabs.lte.model.usage.AggregateRequest;
import com.linklabs.lte.model.usage.UsageHistoryResponse;
import com.linklabs.lte.model.usage.UsageRequest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;

/**
 * Created by vivekmaurya on 1/16/17.
 */
public class VerizonClientTest {

    private static VerizonClient sut;

    static String testApiToken="89ba225e1438e95bd05c3cc288d3591";
    static String name = "username";
    static String password = "password";


    // fill these will real UWS credentials and key and secret from thingspace keysets.
    static String apiKey = "";
    static String apiSecret = "";
//    static String name = "";
//    static String password = "";


    @BeforeClass
    public static void setUp() throws Exception {
      sut = new VerizonClient(name, password, apiKey, apiSecret, testApiToken);
      sut.init();

    }

    @Test
    public void login() throws Exception {
        String token = sut.login();
        System.out.println("token = " + token);
        Assert.assertNotNull(token);
    }


    @Test
    public void activateDevice() throws Exception {
        DeviceId deviceId1 = DeviceIdBuilder
                .builder()
                .id("990013907835573")
                .kind("imei")
                .build();

        DeviceId deviceId2 = DeviceIdBuilder
                .builder()
                .id("89141390780800784259")
                .kind("iccid")
                .build();

        Device device1 = new Device();
        device1.setDeviceIds(Lists.newArrayList(deviceId1, deviceId2));

        ActivateRequest m2m_4G = ActivateRequestBuilder.builder()
                .accountName("TestAccount-1")
                .servicePlan("m2m_4G")
                .mdnZipCode("98801")
                .skuNumber("7640111922001")
                .devices(Lists.newArrayList(device1))
                .build();

        String requestId = sut.activateDevices(m2m_4G);
        System.out.println("requestId = " + requestId);
        Assert.assertNotNull(requestId);
    }


    @Test
    public void listDevice() throws Exception {
        DeviceId deviceId1 = DeviceIdBuilder
                .builder()
                .id("990013907835573")
                .kind("imei")
                .build();


        ListRequest listRequest = new ListRequest();
        listRequest.setDeviceId(deviceId1);
        ListResponse listResponse = sut.listDevices(listRequest);
        System.out.println("listResponse = " + listResponse);
    }


    @Test
    public void suspendDevices() throws Exception {
        DeviceId deviceId1 = DeviceIdBuilder
                .builder()
                .id("990013907835573")
                .kind("imei")
                .build();


        Device device1 = new Device();
        device1.setDeviceIds(Lists.newArrayList(deviceId1));

        SuspendRequest request = new SuspendRequest();
        request.setDevices(Lists.newArrayList(device1));


        String requestId = sut.suspendDevices(request);
        System.out.println("requestId = " + requestId);
        Assert.assertNotNull(requestId);
    }


    @Test
    public void deactivateDevices() throws Exception {
        DeviceId deviceId1 = DeviceIdBuilder
                .builder()
                .id("990013907835573")
                .kind("imei")
                .build();

        Device device1 = new Device();
        device1.setDeviceIds(Lists.newArrayList(deviceId1));

        DeactivateRequest request = new DeactivateRequest();
        request.setDevices(Lists.newArrayList(device1));
        request.setReasonCode("FF");


        String requestId = sut.deactivateDevices(request);
        System.out.println("requestId = " + requestId);
        Assert.assertNotNull(requestId);
    }


    @Test
    public void restoreDevices() throws Exception {
        DeviceId deviceId1 = DeviceIdBuilder
                .builder()
                .id("990013907835573")
                .kind("imei")
                .build();

        Device device1 = new Device();
        device1.setDeviceIds(Lists.newArrayList(deviceId1));

        Device device2 = new Device();
        device2.setDeviceIds(Lists.newArrayList(deviceId1));


        RestoreRequest request = new RestoreRequest();
        request.setDevices(Lists.newArrayList(device1, device2));


        String requestId = sut.restoreDevices(request);
        System.out.println("requestId = " + requestId);
        Assert.assertNotNull(requestId);
    }


    @Test
    public void getUsageHistory() throws Exception {
        DeviceId deviceId1 = DeviceIdBuilder
                .builder()
                .id("990013907835573")
                .kind("imei")
                .build();

        UsageRequest request = new UsageRequest();
        request.setDeviceId(deviceId1);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:MM:mmX")
                .withZone(ZoneOffset.UTC);

        String earliest = formatter.format(Instant.now().minus(Duration.ofDays(30)));
        String latest = formatter.format(Instant.now());
        request.setEarliest(earliest);
        request.setLatest(latest);


        UsageHistoryResponse usageHistory = sut.getUsageHistory(request);
        Assert.assertNotNull(usageHistory);
    }


    @Test
    public void getAggregateUsageHistory() throws Exception {
        DeviceId deviceId1 = DeviceIdBuilder
                .builder()
                .id("990013907835573")
                .kind("imei")
                .build();

        AggregateRequest request = new AggregateRequest();
        request.setDeviceIds(Lists.newArrayList(deviceId1));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX")
                .withZone(ZoneOffset.UTC);
        Instant now = Instant.now();
        String earliest = formatter.format(now.minus(Duration.ofDays(30)));
        String latest = formatter.format(now);
        request.setStartTime(earliest);
        request.setEndTime(latest);


        String requestId = sut.getAggregateUsageHistory(request);
        Assert.assertNotNull(requestId);
    }

    @Test
    public void testSessionToken() throws Exception {
        VerizonClient sessionSut = new VerizonClient(name, password, apiKey, apiSecret, testApiToken, 2);
        sessionSut.init();
        String login = sessionSut.login();
        String again = sessionSut.getSessionToken();
        Assert.assertEquals(login, again);
        Thread.sleep(3000);
        String token = sessionSut.getSessionToken();
        Assert.assertNotEquals(login,token);

    }
}