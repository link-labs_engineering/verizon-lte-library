package com.linklabs.lte;

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;


/**
 * Created by vivekmaurya on 7/11/17.
 */
public class RequestIdTest {

    @Test
    public void testExtractUuid() throws Exception
    {
        VerizonClient sut = new VerizonClient("","","","", "");
        String verizonResponse ="\"text\": \"{\\\"requestId\\\":\\\"595f5c44-c31c-4552-8670-020a1545a84d\\\"}\\n\"";
        Optional<String> uuidFromStr = sut.extractUUIDFromStr(verizonResponse);
        Assert.assertEquals("595f5c44-c31c-4552-8670-020a1545a84d", uuidFromStr.get());
    }

}
