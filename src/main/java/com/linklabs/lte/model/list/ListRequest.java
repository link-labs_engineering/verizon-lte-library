package com.linklabs.lte.model.list;

/**
 * Created by vivekmaurya on 1/17/17.
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.linklabs.lte.model.common.CustomField;
import com.linklabs.lte.model.common.DeviceId;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "accountName",
        "currentState",
        "customFields",
        "deviceId",
        "earliest",
        "groupName",
        "latest",
        "servicePlan"
})
public class ListRequest {

    @JsonProperty("accountName")
    private String accountName;
    @JsonProperty("currentState")
    private String currentState;
    @JsonProperty("customFields")
    private List<CustomField> customFields = null;
    @JsonProperty("deviceId")
    private DeviceId deviceId;
    @JsonProperty("earliest")
    private String earliest;
    @JsonProperty("groupName")
    private String groupName;
    @JsonProperty("latest")
    private String latest;
    @JsonProperty("servicePlan")
    private String servicePlan;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("accountName")
    public String getAccountName() {
        return accountName;
    }

    @JsonProperty("accountName")
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @JsonProperty("currentState")
    public String getCurrentState() {
        return currentState;
    }

    @JsonProperty("currentState")
    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    @JsonProperty("customFields")
    public List<CustomField> getCustomFields() {
        return customFields;
    }

    @JsonProperty("customFields")
    public void setCustomFields(List<CustomField> customFields) {
        this.customFields = customFields;
    }

    @JsonProperty("deviceId")
    public DeviceId getDeviceId() {
        return deviceId;
    }

    @JsonProperty("deviceId")
    public void setDeviceId(DeviceId deviceId) {
        this.deviceId = deviceId;
    }

    @JsonProperty("earliest")
    public String getEarliest() {
        return earliest;
    }

    @JsonProperty("earliest")
    public void setEarliest(String earliest) {
        this.earliest = earliest;
    }

    @JsonProperty("groupName")
    public String getGroupName() {
        return groupName;
    }

    @JsonProperty("groupName")
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @JsonProperty("latest")
    public String getLatest() {
        return latest;
    }

    @JsonProperty("latest")
    public void setLatest(String latest) {
        this.latest = latest;
    }

    @JsonProperty("servicePlan")
    public String getServicePlan() {
        return servicePlan;
    }

    @JsonProperty("servicePlan")
    public void setServicePlan(String servicePlan) {
        this.servicePlan = servicePlan;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}