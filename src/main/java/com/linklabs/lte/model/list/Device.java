package com.linklabs.lte.model.list;

/**
 * Created by vivekmaurya on 1/17/17.
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.linklabs.lte.model.common.CustomField;
import com.linklabs.lte.model.common.DeviceId;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "accountName",
        "billingCycleEndDate",
        "carrierInformations",
        "connected",
        "createdAt",
        "customFields",
        "deviceIds",
        "extendedAttributes",
        "groupNames",
        "ipAddress",
        "lastActivationBy",
        "lastActivationDate",
        "lastConnectionDate"
})
public class Device {

    @JsonProperty("accountName")
    private String accountName;
    @JsonProperty("billingCycleEndDate")
    private String billingCycleEndDate;
    @JsonProperty("carrierInformations")
    private List<CarrierInformation> carrierInformations = null;
    @JsonProperty("connected")
    private Boolean connected;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("customFields")
    private List<CustomField> customFields = null;
    @JsonProperty("deviceIds")
    private List<DeviceId> deviceIds = null;
    @JsonProperty("extendedAttributes")
    private List<ExtendedAttribute> extendedAttributes = null;
    @JsonProperty("groupNames")
    private List<String> groupNames = null;
    @JsonProperty("ipAddress")
    private String ipAddress;
    @JsonProperty("lastActivationBy")
    private String lastActivationBy;
    @JsonProperty("lastActivationDate")
    private String lastActivationDate;
    @JsonProperty("lastConnectionDate")
    private String lastConnectionDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("accountName")
    public String getAccountName() {
        return accountName;
    }

    @JsonProperty("accountName")
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @JsonProperty("billingCycleEndDate")
    public String getBillingCycleEndDate() {
        return billingCycleEndDate;
    }

    @JsonProperty("billingCycleEndDate")
    public void setBillingCycleEndDate(String billingCycleEndDate) {
        this.billingCycleEndDate = billingCycleEndDate;
    }

    @JsonProperty("carrierInformations")
    public List<CarrierInformation> getCarrierInformations() {
        return carrierInformations;
    }

    @JsonProperty("carrierInformations")
    public void setCarrierInformations(List<CarrierInformation> carrierInformations) {
        this.carrierInformations = carrierInformations;
    }

    @JsonProperty("connected")
    public Boolean getConnected() {
        return connected;
    }

    @JsonProperty("connected")
    public void setConnected(Boolean connected) {
        this.connected = connected;
    }

    @JsonProperty("createdAt")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("createdAt")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("customFields")
    public List<CustomField> getCustomFields() {
        return customFields;
    }

    @JsonProperty("customFields")
    public void setCustomFields(List<CustomField> customFields) {
        this.customFields = customFields;
    }

    @JsonProperty("deviceIds")
    public List<DeviceId> getDeviceIds() {
        return deviceIds;
    }

    @JsonProperty("deviceIds")
    public void setDeviceIds(List<DeviceId> deviceIds) {
        this.deviceIds = deviceIds;
    }

    @JsonProperty("extendedAttributes")
    public List<ExtendedAttribute> getExtendedAttributes() {
        return extendedAttributes;
    }

    @JsonProperty("extendedAttributes")
    public void setExtendedAttributes(List<ExtendedAttribute> extendedAttributes) {
        this.extendedAttributes = extendedAttributes;
    }

    @JsonProperty("groupNames")
    public List<String> getGroupNames() {
        return groupNames;
    }

    @JsonProperty("groupNames")
    public void setGroupNames(List<String> groupNames) {
        this.groupNames = groupNames;
    }

    @JsonProperty("ipAddress")
    public String getIpAddress() {
        return ipAddress;
    }

    @JsonProperty("ipAddress")
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @JsonProperty("lastActivationBy")
    public String getLastActivationBy() {
        return lastActivationBy;
    }

    @JsonProperty("lastActivationBy")
    public void setLastActivationBy(String lastActivationBy) {
        this.lastActivationBy = lastActivationBy;
    }

    @JsonProperty("lastActivationDate")
    public String getLastActivationDate() {
        return lastActivationDate;
    }

    @JsonProperty("lastActivationDate")
    public void setLastActivationDate(String lastActivationDate) {
        this.lastActivationDate = lastActivationDate;
    }

    @JsonProperty("lastConnectionDate")
    public String getLastConnectionDate() {
        return lastConnectionDate;
    }

    @JsonProperty("lastConnectionDate")
    public void setLastConnectionDate(String lastConnectionDate) {
        this.lastConnectionDate = lastConnectionDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}