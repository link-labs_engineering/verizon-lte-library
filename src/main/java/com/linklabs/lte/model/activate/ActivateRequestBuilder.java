package com.linklabs.lte.model.activate;

import com.linklabs.lte.model.common.CustomField;
import com.linklabs.lte.model.common.Device;

import java.util.List;

public class ActivateRequestBuilder {
    private String accountName;
    private String carrierIpPoolName;
    private String carrierName;
    private String costCenterCode;
    private List<CustomField> customFields;
    private List<Device> devices;
    private String groupName;
    private String leadId;
    private String mdnZipCode;
    private PrimaryPlaceOfUse primaryPlaceOfUse;
    private String publicIpRestriction;
    private String servicePlan;
    private String skuNumber;

    public ActivateRequestBuilder accountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    public ActivateRequestBuilder carrierIpPoolName(String carrierIpPoolName) {
        this.carrierIpPoolName = carrierIpPoolName;
        return this;
    }

    public ActivateRequestBuilder carrierName(String carrierName) {
        this.carrierName = carrierName;
        return this;
    }

    public ActivateRequestBuilder costCenterCode(String costCenterCode) {
        this.costCenterCode = costCenterCode;
        return this;
    }

    public ActivateRequestBuilder customFields(List<CustomField> customFields) {
        this.customFields = customFields;
        return this;
    }

    public ActivateRequestBuilder devices(List<Device> devices) {
        this.devices = devices;
        return this;
    }

    public ActivateRequestBuilder groupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public ActivateRequestBuilder leadId(String leadId) {
        this.leadId = leadId;
        return this;
    }

    public ActivateRequestBuilder mdnZipCode(String mdnZipCode) {
        this.mdnZipCode = mdnZipCode;
        return this;
    }

    public ActivateRequestBuilder primaryPlaceOfUse(PrimaryPlaceOfUse primaryPlaceOfUse) {
        this.primaryPlaceOfUse = primaryPlaceOfUse;
        return this;
    }

    public ActivateRequestBuilder publicIpRestriction(String publicIpRestriction) {
        this.publicIpRestriction = publicIpRestriction;
        return this;
    }

    public ActivateRequestBuilder servicePlan(String servicePlan) {
        this.servicePlan = servicePlan;
        return this;
    }

    public ActivateRequestBuilder skuNumber(String skuNumber) {
        this.skuNumber = skuNumber;
        return this;
    }

    public ActivateRequest build() {
        return new ActivateRequest(accountName, carrierIpPoolName, carrierName, costCenterCode, customFields, devices, groupName, leadId, mdnZipCode, primaryPlaceOfUse, publicIpRestriction, servicePlan, skuNumber);
    }

    public static ActivateRequestBuilder builder(){
        return new ActivateRequestBuilder();
    }
}