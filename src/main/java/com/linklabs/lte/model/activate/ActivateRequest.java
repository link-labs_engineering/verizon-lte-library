package com.linklabs.lte.model.activate;

/**
 * Created by vivekmaurya on 1/16/17.
 */
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.linklabs.lte.model.common.CustomField;
import com.linklabs.lte.model.common.Device;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "accountName",
        "carrierIpPoolName",
        "carrierName",
        "costCenterCode",
        "customFields",
        "devices",
        "groupName",
        "leadId",
        "mdnZipCode",
        "primaryPlaceOfUse",
        "publicIpRestriction",
        "servicePlan",
        "skuNumber"
})
public class ActivateRequest {

    @JsonProperty("accountName")
    private String accountName;
    @JsonProperty("carrierIpPoolName")
    private String carrierIpPoolName;
    @JsonProperty("carrierName")
    private String carrierName;
    @JsonProperty("costCenterCode")
    private String costCenterCode;
    @JsonProperty("customFields")
    private List<CustomField> customFields = null;
    @JsonProperty("devices")
    private List<Device> devices = null;
    @JsonProperty("groupName")
    private String groupName;
    @JsonProperty("leadId")
    private String leadId;
    @JsonProperty("mdnZipCode")
    private String mdnZipCode;
    @JsonProperty("primaryPlaceOfUse")
    private PrimaryPlaceOfUse primaryPlaceOfUse;
    @JsonProperty("publicIpRestriction")
    private String publicIpRestriction;
    @JsonProperty("servicePlan")
    private String servicePlan;
    @JsonProperty("skuNumber")
    private String skuNumber;

    @JsonProperty("accountName")
    public String getAccountName() {
        return accountName;
    }

    @JsonProperty("accountName")
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @JsonProperty("carrierIpPoolName")
    public String getCarrierIpPoolName() {
        return carrierIpPoolName;
    }

    @JsonProperty("carrierIpPoolName")
    public void setCarrierIpPoolName(String carrierIpPoolName) {
        this.carrierIpPoolName = carrierIpPoolName;
    }

    @JsonProperty("carrierName")
    public String getCarrierName() {
        return carrierName;
    }

    @JsonProperty("carrierName")
    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    @JsonProperty("costCenterCode")
    public String getCostCenterCode() {
        return costCenterCode;
    }

    @JsonProperty("costCenterCode")
    public void setCostCenterCode(String costCenterCode) {
        this.costCenterCode = costCenterCode;
    }

    @JsonProperty("customFields")
    public List<CustomField> getCustomFields() {
        return customFields;
    }

    @JsonProperty("customFields")
    public void setCustomFields(List<CustomField> customFields) {
        this.customFields = customFields;
    }

    @JsonProperty("devices")
    public List<Device> getDevices() {
        return devices;
    }

    @JsonProperty("devices")
    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    @JsonProperty("groupName")
    public String getGroupName() {
        return groupName;
    }

    @JsonProperty("groupName")
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @JsonProperty("leadId")
    public String getLeadId() {
        return leadId;
    }

    @JsonProperty("leadId")
    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    @JsonProperty("mdnZipCode")
    public String getMdnZipCode() {
        return mdnZipCode;
    }

    @JsonProperty("mdnZipCode")
    public void setMdnZipCode(String mdnZipCode) {
        this.mdnZipCode = mdnZipCode;
    }

    @JsonProperty("primaryPlaceOfUse")
    public PrimaryPlaceOfUse getPrimaryPlaceOfUse() {
        return primaryPlaceOfUse;
    }

    @JsonProperty("primaryPlaceOfUse")
    public void setPrimaryPlaceOfUse(PrimaryPlaceOfUse primaryPlaceOfUse) {
        this.primaryPlaceOfUse = primaryPlaceOfUse;
    }

    @JsonProperty("publicIpRestriction")
    public String getPublicIpRestriction() {
        return publicIpRestriction;
    }

    @JsonProperty("publicIpRestriction")
    public void setPublicIpRestriction(String publicIpRestriction) {
        this.publicIpRestriction = publicIpRestriction;
    }

    @JsonProperty("servicePlan")
    public String getServicePlan() {
        return servicePlan;
    }

    @JsonProperty("servicePlan")
    public void setServicePlan(String servicePlan) {
        this.servicePlan = servicePlan;
    }

    @JsonProperty("skuNumber")
    public String getSkuNumber() {
        return skuNumber;
    }

    @JsonProperty("skuNumber")
    public void setSkuNumber(String skuNumber) {
        this.skuNumber = skuNumber;
    }

    public ActivateRequest(String accountName, String carrierIpPoolName, String carrierName, String costCenterCode, List<CustomField> customFields, List<Device> devices, String groupName, String leadId, String mdnZipCode, PrimaryPlaceOfUse primaryPlaceOfUse, String publicIpRestriction, String servicePlan, String skuNumber) {
        this.accountName = accountName;
        this.carrierIpPoolName = carrierIpPoolName;
        this.carrierName = carrierName;
        this.costCenterCode = costCenterCode;
        this.customFields = customFields;
        this.devices = devices;
        this.groupName = groupName;
        this.leadId = leadId;
        this.mdnZipCode = mdnZipCode;
        this.primaryPlaceOfUse = primaryPlaceOfUse;
        this.publicIpRestriction = publicIpRestriction;
        this.servicePlan = servicePlan;
        this.skuNumber = skuNumber;
    }

    public ActivateRequest() {
    }
}
