package com.linklabs.lte.model.activate;

/**
 * Created by vivekmaurya on 1/16/17.
 */
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "address",
        "customerName"
})
public class PrimaryPlaceOfUse {

    @JsonProperty("address")
    private Address address;
    @JsonProperty("customerName")
    private CustomerName customerName;

    @JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    @JsonProperty("customerName")
    public CustomerName getCustomerName() {
        return customerName;
    }

    @JsonProperty("customerName")
    public void setCustomerName(CustomerName customerName) {
        this.customerName = customerName;
    }

}