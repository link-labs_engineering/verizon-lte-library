package com.linklabs.lte.model.common;

/**
 * Created by vivekmaurya on 1/16/17.
 */
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "kind"
})
public class DeviceId {

    @JsonProperty("id")
    private String id;
    @JsonProperty("kind")
    private String kind;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("kind")
    public String getKind() {
        return kind;
    }

    public DeviceId(String id, String kind) {
        this.id = id;
        this.kind = kind;
    }

    @JsonProperty("kind")
    public void setKind(String kind) {
        this.kind = kind;
    }

    public DeviceId() {
    }
}