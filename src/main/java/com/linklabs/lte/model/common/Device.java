package com.linklabs.lte.model.common;

/**
 * Created by vivekmaurya on 1/16/17.
 */
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.linklabs.lte.model.common.DeviceId;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "deviceIds"
})
public class Device {

    @JsonProperty("deviceIds")
    private List<DeviceId> deviceIds = null;

    @JsonProperty("deviceIds")
    public List<DeviceId> getDeviceIds() {
        return deviceIds;
    }

    @JsonProperty("deviceIds")
    public void setDeviceIds(List<DeviceId> deviceIds) {
        this.deviceIds = deviceIds;
    }

}