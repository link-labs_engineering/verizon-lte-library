package com.linklabs.lte.model.common;

public class DeviceIdBuilder {
    private String id;
    private String kind;

    public DeviceIdBuilder id(String id) {
        this.id = id;
        return this;
    }

    public DeviceIdBuilder kind(String kind) {
        this.kind = kind;
        return this;
    }

    public DeviceId build() {
        return new DeviceId(id, kind);
    }

    public static DeviceIdBuilder builder(){
        return new DeviceIdBuilder();
    }
}