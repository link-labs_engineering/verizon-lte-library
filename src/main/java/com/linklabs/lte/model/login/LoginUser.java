package com.linklabs.lte.model.login;

/**
 * Created by vivekmaurya on 1/16/17.
 */
public class LoginUser {

    String password;

    String username;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
