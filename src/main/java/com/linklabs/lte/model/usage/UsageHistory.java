package com.linklabs.lte.model.usage;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.linklabs.lte.model.list.ExtendedAttribute;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "bytesUsed",
        "extendedAttributes",
        "servicePlan",
        "smsUsed",
        "source",
        "timestamp"
})
public class UsageHistory {

    @JsonProperty("bytesUsed")
    private Integer bytesUsed;
    @JsonProperty("extendedAttributes")
    private List<ExtendedAttribute> extendedAttributes = null;
    @JsonProperty("servicePlan")
    private String servicePlan;
    @JsonProperty("smsUsed")
    private Integer smsUsed;
    @JsonProperty("source")
    private String source;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("bytesUsed")
    public Integer getBytesUsed() {
        return bytesUsed;
    }

    @JsonProperty("bytesUsed")
    public void setBytesUsed(Integer bytesUsed) {
        this.bytesUsed = bytesUsed;
    }

    @JsonProperty("extendedAttributes")
    public List<ExtendedAttribute> getExtendedAttributes() {
        return extendedAttributes;
    }

    @JsonProperty("extendedAttributes")
    public void setExtendedAttributes(List<ExtendedAttribute> extendedAttributes) {
        this.extendedAttributes = extendedAttributes;
    }

    @JsonProperty("servicePlan")
    public String getServicePlan() {
        return servicePlan;
    }

    @JsonProperty("servicePlan")
    public void setServicePlan(String servicePlan) {
        this.servicePlan = servicePlan;
    }

    @JsonProperty("smsUsed")
    public Integer getSmsUsed() {
        return smsUsed;
    }

    @JsonProperty("smsUsed")
    public void setSmsUsed(Integer smsUsed) {
        this.smsUsed = smsUsed;
    }

    @JsonProperty("source")
    public String getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(String source) {
        this.source = source;
    }

    @JsonProperty("timestamp")
    public String getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}