package com.linklabs.lte.model.usage;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.linklabs.lte.model.common.DeviceId;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "deviceId",
        "earliest",
        "latest"
})
public class UsageRequest {

    @JsonProperty("deviceId")
    private DeviceId deviceId;
    @JsonProperty("earliest")
    private String earliest;
    @JsonProperty("latest")
    private String latest;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("deviceId")
    public DeviceId getDeviceId() {
        return deviceId;
    }

    @JsonProperty("deviceId")
    public void setDeviceId(DeviceId deviceId) {
        this.deviceId = deviceId;
    }

    @JsonProperty("earliest")
    public String getEarliest() {
        return earliest;
    }

    @JsonProperty("earliest")
    public void setEarliest(String earliest) {
        this.earliest = earliest;
    }

    @JsonProperty("latest")
    public String getLatest() {
        return latest;
    }

    @JsonProperty("latest")
    public void setLatest(String latest) {
        this.latest = latest;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}