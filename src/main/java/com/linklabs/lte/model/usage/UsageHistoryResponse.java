package com.linklabs.lte.model.usage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.linklabs.lte.model.VerizonResponse;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "hasMoreData",
        "usageHistory"
})
public class UsageHistoryResponse extends VerizonResponse{

    @JsonProperty("hasMoreData")
    private Boolean hasMoreData;
    @JsonProperty("usageHistory")
    private List<UsageHistory> usageHistory = null;

    @JsonProperty("hasMoreData")
    public Boolean getHasMoreData() {
        return hasMoreData;
    }

    @JsonProperty("hasMoreData")
    public void setHasMoreData(Boolean hasMoreData) {
        this.hasMoreData = hasMoreData;
    }

    @JsonProperty("usageHistory")
    public List<UsageHistory> getUsageHistory() {
        return usageHistory;
    }

    @JsonProperty("usageHistory")
    public void setUsageHistory(List<UsageHistory> usageHistory) {
        this.usageHistory = usageHistory;
    }


}
