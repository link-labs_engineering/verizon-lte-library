package com.linklabs.lte.model.deactivate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.linklabs.lte.model.common.CustomField;
import com.linklabs.lte.model.common.Device;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "accountName",
        "customFields",
        "devices",
        "etfWaiver",
        "groupName",
        "reasonCode",
        "servicePlan"
})
public class DeactivateRequest {

    @JsonProperty("accountName")
    private String accountName;
    @JsonProperty("customFields")
    private List<CustomField> customFields = null;
    @JsonProperty("devices")
    private List<Device> devices = null;
    @JsonProperty("etfWaiver")
    private Boolean etfWaiver;
    @JsonProperty("groupName")
    private String groupName;
    @JsonProperty("reasonCode")
    private String reasonCode;
    @JsonProperty("servicePlan")
    private String servicePlan;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("accountName")
    public String getAccountName() {
        return accountName;
    }

    @JsonProperty("accountName")
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @JsonProperty("customFields")
    public List<CustomField> getCustomFields() {
        return customFields;
    }

    @JsonProperty("customFields")
    public void setCustomFields(List<CustomField> customFields) {
        this.customFields = customFields;
    }

    @JsonProperty("devices")
    public List<Device> getDevices() {
        return devices;
    }

    @JsonProperty("devices")
    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    @JsonProperty("etfWaiver")
    public Boolean getEtfWaiver() {
        return etfWaiver;
    }

    @JsonProperty("etfWaiver")
    public void setEtfWaiver(Boolean etfWaiver) {
        this.etfWaiver = etfWaiver;
    }

    @JsonProperty("groupName")
    public String getGroupName() {
        return groupName;
    }

    @JsonProperty("groupName")
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @JsonProperty("reasonCode")
    public String getReasonCode() {
        return reasonCode;
    }

    @JsonProperty("reasonCode")
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    @JsonProperty("servicePlan")
    public String getServicePlan() {
        return servicePlan;
    }

    @JsonProperty("servicePlan")
    public void setServicePlan(String servicePlan) {
        this.servicePlan = servicePlan;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}