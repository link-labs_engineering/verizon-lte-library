package com.linklabs.lte;

/**
 * Created by vivekmaurya on 1/17/17.
 */
public class LteApiException extends RuntimeException {

    String httpStatusCode;

    String errorCode;

    public LteApiException(String message, Throwable cause, String httpStatusCode, String errorCode) {
        super(message, cause);
        this.httpStatusCode = httpStatusCode;
        this.errorCode = errorCode;
    }

    public LteApiException(String message, String errorCode) {
        super(message, null);
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        return "LteApiException{" +
                "httpStatusCode='" + httpStatusCode + '\'' +
                ", errorCode='" + errorCode + '\'' +
                '}';
    }
}
