package com.linklabs.lte;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Strings;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableMap;
import com.linklabs.lte.model.*;
import com.linklabs.lte.model.activate.ActivateRequest;
import com.linklabs.lte.model.common.DeviceId;
import com.linklabs.lte.model.deactivate.DeactivateRequest;
import com.linklabs.lte.model.list.ListRequest;
import com.linklabs.lte.model.list.ListResponse;
import com.linklabs.lte.model.login.APITokenResponse;
import com.linklabs.lte.model.login.LoginResponse;
import com.linklabs.lte.model.login.LoginUser;
import com.linklabs.lte.model.restore.RestoreRequest;
import com.linklabs.lte.model.suspend.SuspendRequest;
import com.linklabs.lte.model.usage.AggregateRequest;
import com.linklabs.lte.model.usage.UsageHistoryResponse;
import com.linklabs.lte.model.usage.UsageRequest;
import com.linklabs.lte.service.ConnectivityService;
import com.linklabs.lte.service.LoginService;
import com.linklabs.lte.service.TokenService;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by vivekmaurya on 1/16/17.
 */
public class VerizonClient {

    private static final Logger log = LoggerFactory.getLogger(VerizonClient.class);

    private static final String SESSION_TOKEN_KEY = "sessionToken";

    private static final String TOKENS = "tokens";

    private static final String API_TOKEN_KEY = "apiToken";

    // 15 minutes of session timeout
    private static final int DEFAULT_TIMEOUT = 15 * 60 ;


    private String userName;

    private String password;

    private Optional<String> apiToken = Optional.empty();

    private Retrofit retrofit;

    private Retrofit tokenRetrofit;

    private String apiKey;

    private String apiSecret;

    private ConnectivityService connectivityService;

    private LoginService loginService;

    private TokenService tokenService;

    private Cache<String, Map<String, String>> tokenCache;

    private int sessionTimeoutSeconds;

    public static final String UUID_REGEX = "\\p{XDigit}{8}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{12}";

    public static final Pattern uuidRegex = Pattern.compile(UUID_REGEX);

    /**
     *
     * @param userName
     * @param password
     * @param apiKey
     * @param apiSecret
     * @param testApiToken
     * @param sessionTimeoutSeconds
     */
    public VerizonClient(String userName, String password, String apiKey, String apiSecret, String testApiToken, int sessionTimeoutSeconds) {
        this.userName = userName;
        this.password = password;
        // override if the test API token is present. Dont want to add different beans for test and use spring profile.
        // If testApiToken is present, it means use testToken. For prod, this should be empty
        if(!Strings.isNullOrEmpty(testApiToken)){
            this.apiToken = Optional.of("Bearer " + testApiToken);
        }
        this.apiKey = apiKey;
        this.apiSecret = apiSecret;
        this.sessionTimeoutSeconds = sessionTimeoutSeconds;
    }

    /**
     * This constructor should be used
     * @param userName
     * @param password
     * @param apiKey
     * @param apiSecret
     */
    public VerizonClient(String userName, String password, String apiKey, String apiSecret, String testApiToken) {
        this(userName, password, apiKey, apiSecret, testApiToken, DEFAULT_TIMEOUT);
    }

    /**
     * initializes the client. Should be called once after the class
     * creation.
     */
    public void init(){
        log.info("Initializing Verizon Client");
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl("https://thingspace.verizon.com/api/m2m/v1/")
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        tokenRetrofit = new Retrofit.Builder()
                .baseUrl("https://thingspace.verizon.com/api/ts/v1/")
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        tokenService = tokenRetrofit.create(TokenService.class);

        loginService = retrofit.create(LoginService.class);

        connectivityService = retrofit.create(ConnectivityService.class);

        // Expire key after 15 minutes of inactivity
        tokenCache = CacheBuilder.newBuilder()
                .expireAfterAccess(sessionTimeoutSeconds, TimeUnit.SECONDS)
                .maximumSize(1)
                .build();
    }

    private String getHourlyApiToken(){
        if(apiToken.isPresent()){
            return apiToken.get();
        }
        String basicAuth = Credentials.basic(apiKey, apiSecret);
        Call<APITokenResponse> getApiToken = tokenService.getAccessToken(basicAuth, "client_credentials");
        Response<APITokenResponse> execute = invokeSync(getApiToken);
        return "Bearer " + execute.body().getAccessToken();
    }


    /**
     * API login and creates a session token that can be used for sometime.
     * @return sessionToken
     */
    public String login(){
        return internal_login().get(SESSION_TOKEN_KEY);
    }


    private Map<String, String> internal_login() {
        String apiToken = getHourlyApiToken();
        LoginUser lu = new LoginUser();
        lu.setPassword(password);
        lu.setUsername(userName);
        Call<LoginResponse> login = loginService.login(apiToken, lu);
        Response<LoginResponse> execute = invokeSync(login);
        ImmutableMap<String, String> tokens = ImmutableMap.of(SESSION_TOKEN_KEY, execute.body().getSessionToken(), API_TOKEN_KEY, apiToken);
        tokenCache.put(TOKENS, tokens);
        return tokens;
    }

    
    public void checkDevice(String id, String type){
        ListRequest listRequest = new ListRequest();       
        listRequest.setDeviceId(new DeviceId(id, type));
        Call<ListResponse> response = connectivityService.listDevice(getApiToken(), getSessionToken(), listRequest);
        log.info(response.toString());
    }
    /**
     *
     * Activates the input devices in the request. Returns a requestId that will used for the callback when the activation
     * request is completed.
     * @param activateRequest
     * @return requestId
     */
    public String activateDevices(ActivateRequest activateRequest){
        Call<VerizonResponse> verizonResponseCall = connectivityService.activateDevices(getApiToken(), getSessionToken(), activateRequest);
        Response<VerizonResponse> execute = invokeSync(verizonResponseCall);
        return getRequestId(execute);
    }

    /**
     * Returns information about a single or group of devices.
     * @param listRequest
     * @return ListResponse
     */
    //TODO handle more data
    public ListResponse listDevices(ListRequest listRequest){
        Call<ListResponse> listResponseCall = connectivityService.listDevice(getApiToken(), getSessionToken(), listRequest);
        Response<ListResponse> execute = invokeSync(listResponseCall);
        return execute.body();
    }


    /**
     *
     * Suspends the input devices in the request. Returns a requestId that will used for the callback when the suspend
     * request is completed.
     * @param suspendRequest
     * @return requestId
     */
    public String suspendDevices(SuspendRequest suspendRequest){
        Call<VerizonResponse> verizonResponseCall = connectivityService.suspendDevices(getApiToken(), getSessionToken(), suspendRequest);
        Response<VerizonResponse> execute = invokeSync(verizonResponseCall);
        return getRequestId(execute);
    }

    /**
     *
     * Suspends the input devices in the request. Returns a requestId that will used for the callback when the deactivate
     * request is completed.
     * @param deactivateRequest
     * @return requestId
     */
    public String deactivateDevices(DeactivateRequest deactivateRequest){
        Call<VerizonResponse> verizonResponseCall = connectivityService.deactivateDevices(getApiToken(), getSessionToken(), deactivateRequest);
        Response<VerizonResponse> execute = invokeSync(verizonResponseCall);
        return getRequestId(execute);
    }


    /**
     *
     * Restores service to one or more suspended devices. Returns a requestId that will used for the callback when the restore
     * request is completed.
     * @param restoreRequest
     * @return requestId
     */
    public String restoreDevices(RestoreRequest restoreRequest){
        Call<VerizonResponse> verizonResponseCall = connectivityService.restoreDevices(getApiToken(), getSessionToken(), restoreRequest);
        Response<VerizonResponse> execute = invokeSync(verizonResponseCall);
        return getRequestId(execute);
    }


    /**
     *
     * Returns the usage history of a device between the supplied dates
     * @param usageRequest
     * @return UsageHistoryResponse
     */
    public UsageHistoryResponse getUsageHistory(UsageRequest usageRequest){
        Call<UsageHistoryResponse> verizonResponseCall = connectivityService.getUsageHistoryDevice(getApiToken(), getSessionToken(), usageRequest);
        Response<UsageHistoryResponse> execute = invokeSync(verizonResponseCall);
        return execute.body();
    }


    /**
     *
     * Returns the aggregate usage history of Device/Devices between the supplied dates
     * @param aggregateRequest
     * @return RequestId
     */
    public String getAggregateUsageHistory(AggregateRequest aggregateRequest){
        Call<VerizonResponse> verizonResponseCall = connectivityService.getAggregateUsage(getApiToken(), getSessionToken(), aggregateRequest);
        Response<VerizonResponse> execute = invokeSync(verizonResponseCall);
        return getRequestId(execute);
    }

    private String getRequestId(Response<VerizonResponse> response){
        Map<String, Object> additionalProperties = response.body().getAdditionalProperties();
        if(additionalProperties.containsKey("requestId")){
            return additionalProperties.get("requestId").toString();
        }
        else{
            //fall back on text ..sad :-(
            String text = additionalProperties.get("text").toString();
            Optional<String> requestIdFromText = extractUUIDFromStr(text);
            if(requestIdFromText.isPresent()){
                return requestIdFromText.get();
            }

        }
        throw new RuntimeException("Cannot get request id from Verizon Response.");
    }


    private LteApiException getException(String message, Throwable ex, String code, ResponseBody errorBody){
        try{
            return new LteApiException(String.format("Error %s: ", message), ex, code, errorBody.string());
        }catch(IOException ioex){
            log.error("Error",ioex);
        }
        return new LteApiException("Error Activating device ", null,code, null);
    }

    private <T extends VerizonResponse> Response<T> invokeSync(Call<T> call){
        try{
            Response<T> response =  call.execute();
            log.info(ToStringBuilder.reflectionToString(response));
            
            if(response.raw().code() != 200){
                throw getException("Api error", null, String.valueOf(response.raw().code()), response.errorBody());
            }
            if(response.body().getErrorCode() != null && response.body().getErrorCode().equals("REQUEST_FAILED.SessionToken.Expired")){
                tokenCache.invalidate(TOKENS);
                throw new LteApiException(response.body().getErrorMessage(),response.body().getErrorCode());
            }
            if(response.body().getErrorCode() != null){
                throw new LteApiException(response.body().getErrorMessage(),response.body().getErrorCode());
            }
            
            return response;
        }catch(IOException ex){
            log.warn("unhandled exception.  will invalidate the tokenCache and continue");
            tokenCache.invalidate(TOKENS);
            throw new LteApiException("Api error", ex, "0", ex.getMessage());
        }
    }

    @VisibleForTesting
    Optional<String> extractUUIDFromStr(String badResponse){
        Matcher matcher = uuidRegex.matcher(badResponse);

        while (matcher.find()) {
            String requestId = matcher.group(0);
            return Optional.of(requestId);
        }
        return Optional.empty();
    }

    @VisibleForTesting
    String getSessionToken() {
        try {
            Map<String, String> tokens = tokenCache.get(TOKENS, () -> internal_login());
            return tokens.get(SESSION_TOKEN_KEY);
        } catch (ExecutionException e) {
            throw new LteApiException("Unable to get session token.","");
        }
    }

    @VisibleForTesting
    String getApiToken() {
        try {
            // if apiToken is fixed. Used normally in test.
            if(apiToken.isPresent()){
                return apiToken.get();
            }
            Map<String, String> tokens = tokenCache.get(TOKENS, () -> internal_login());
            return tokens.get(API_TOKEN_KEY);
        } catch (ExecutionException e) {
            throw new LteApiException("Unable to get session token.","");
        }
    }


}
