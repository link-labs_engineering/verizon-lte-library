package com.linklabs.lte.service;

import com.linklabs.lte.model.login.APITokenResponse;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * Created by vivekmaurya on 1/16/17.
 */
public interface TokenService {

    @FormUrlEncoded
    @POST("oauth2/token")
    Call<APITokenResponse> getAccessToken(@Header("Authorization") String basicAuth, @Field("grant_type") String grantType);
}
