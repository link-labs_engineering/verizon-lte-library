package com.linklabs.lte.service;

import com.linklabs.lte.model.login.LoginResponse;
import com.linklabs.lte.model.login.LoginUser;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by vivekmaurya on 1/16/17.
 */
public interface LoginService {

    @POST("session/login")
    Call<LoginResponse> login(@Header("Authorization") String token, @Body LoginUser loginUser);
}
