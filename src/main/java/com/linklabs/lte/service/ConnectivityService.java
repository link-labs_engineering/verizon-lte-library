package com.linklabs.lte.service;

import com.linklabs.lte.model.VerizonResponse;
import com.linklabs.lte.model.activate.ActivateRequest;
import com.linklabs.lte.model.deactivate.DeactivateRequest;
import com.linklabs.lte.model.list.ListRequest;
import com.linklabs.lte.model.list.ListResponse;
import com.linklabs.lte.model.restore.RestoreRequest;
import com.linklabs.lte.model.suspend.SuspendRequest;
import com.linklabs.lte.model.usage.AggregateRequest;
import com.linklabs.lte.model.usage.UsageHistoryResponse;
import com.linklabs.lte.model.usage.UsageRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by vivekmaurya on 1/17/17.
 */
public interface ConnectivityService {

    @POST("devices/actions/activate")
    Call<VerizonResponse> activateDevices(@Header("Authorization") String token, @Header("VZ-M2M-Token") String sessionToken, @Body ActivateRequest activateRequest);


    @POST("devices/actions/list")
    Call<ListResponse> listDevice(@Header("Authorization") String token, @Header("VZ-M2M-Token") String sessionToken, @Body ListRequest listRequest);


    @POST("devices/actions/suspend")
    Call<VerizonResponse> suspendDevices(@Header("Authorization") String token, @Header("VZ-M2M-Token") String sessionToken, @Body SuspendRequest suspendRequest);


    @POST("devices/actions/deactivate")
    Call<VerizonResponse> deactivateDevices(@Header("Authorization") String token, @Header("VZ-M2M-Token") String sessionToken, @Body DeactivateRequest deactivateRequest);


    @POST("devices/actions/restore")
    Call<VerizonResponse> restoreDevices(@Header("Authorization") String token, @Header("VZ-M2M-Token") String sessionToken, @Body RestoreRequest restoreRequest);


    @POST("devices/usage/actions/list")
    Call<UsageHistoryResponse> getUsageHistoryDevice(@Header("Authorization") String token, @Header("VZ-M2M-Token") String sessionToken, @Body UsageRequest usageRequest);


    @POST("devices/usage/actions/list/aggregate")
    Call<VerizonResponse> getAggregateUsage(@Header("Authorization") String token, @Header("VZ-M2M-Token") String sessionToken, @Body AggregateRequest aggregateRequest);
}
